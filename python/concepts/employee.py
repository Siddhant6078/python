# CRUD in Python using Sqlite3

class Employee:
	"""A sample Employee class"""

	def __init__(self,first,last,pay):
		self.first = first
		self.last = last
		self.pay = pay

	@property
	def email(self):
		return '{}.{}@email.com'.format(self.first,self.last)

	@property
	def fullname(self):
		return '{} {}'.format(self.first,self.last)

	def __repr__(self):
		return "Employee('{}', '{}', {})".format(self.first,self.last,self.pay)

# emp_1 = Employee('John','Smith')

# emp_1.fullname = 'Sid Chaudhari'

# print(emp_1.first)
# print(emp_1.email)
# print(emp_1.fullname)

# del emp_1.fullname