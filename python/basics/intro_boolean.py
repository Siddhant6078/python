# language = 'Java'
# if language == 'Python':
# 	print('Language is Python')
# elif language == 'Java':
# 	print('Language is Java')
# elif language == 'JavaScript':
# 	print('Language is JavaScript')
# else:
# 	print('No match')

# and or not
# user = 'Admin'
# logged_in = False

# if not logged_in:
# 	print('Please Log In')
# else:
# 	print('Welcome')

# a = [1,2,3]
# b = a
# print(id(a))
# print(id(b))
# print(id(a) == id(b))

condition = 'Test'

if condition:
	print('Evaluated to True')
else:
	print('Evaluated to False')