import selenium.webdriver as webdriver

def get_results(search_terms):
	url = "https://scholar.google.co.in/"
	browser = webdriver.Firefox()
	browser.get(url)
	search_box = browser.find_element_by_id("gs_hdr_tsi")
	search_box.send_keys(search_terms)
	search_box.submit()
	try:
		links = browser.find_element_by_xpath("//ol[@class='web_regular_results']//h3//a")
	except:
		links = browser.find_element_by_xpath("//h3//a")
	results = []
	for link in links:
		href = link.get_attribute("href")
		print(href)
		results.append(href)
	browser.close()
	return results

get_results("recommendation systems pccoe")